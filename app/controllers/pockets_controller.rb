class PocketsController < ApplicationController
    def new
        @pocket = Pocket.new
    end

    def create
        pocket = Pocket.new (resources_params)
        pocket.save 
        redirect_to pockets_path
        flash[:notice] = 'Pocket has been created'
    end

    def edit
        @pocket = Pocket.find(params[:id])
    end

    def update
        @pocket = Pocket.find(params[:id])
        @pocket.update(resources_params)
        flash[:notice] = 'Pocket has been update'
        redirect_to pockets_path(@pocket)
    end

    def destroy
        @pocket = Pocket.find(params[:id])
        @pocket.destroy
        flash[:notice] = 'Pocket has been destroy'
        redirect_to pockets_path(@exam)
    end

    def index
        @pockets = Pocket.all
    end
    
    def show
        id = params[:id]
        @pocket = Pocket.find(id)
    end

    private  
    def resources_params
        params.required(:pocket).permit(:balance, :student_id, :teacher_id)
    end
end
