class ReportsController < ApplicationController
    def new
        @report = Report.new
    end

    def create
        report = Report.new (resources_params)
        report.save 
        redirect_to reports_path
        flash[:notice] = 'Report has been created'
    end

    def edit
        @report = Report.find(params[:id])
    end

    def update
        @report = Report.find(params[:id])
        @report.update(resources_params)
        flash[:notice] = 'Report has been update'
        redirect_to reports_path(@report)
    end

    def destroy
        @report = Report.find(params[:id])
        @report.destroy
        flash[:notice] = 'Exam has been destroy'
        redirect_to reports_path(@report)
    end

    def index
        @reports = Report.all
    end
    
    def show
        id = params[:id]
        @report = Report.find(id)
    end

    private  
    def resources_params
        params.required(:report).permit(:title, :hasil, :mapel, :teacher_id, :student_id)
    end
end
