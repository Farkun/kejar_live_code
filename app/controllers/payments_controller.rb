class PaymentsController < ApplicationController
    def new
        @payment = Payment.new
    end

    def create
        payment = Payment.new (resources_params)
        payment.save 
        redirect_to payments_path
        flash[:notice] = 'Payment has been created'
    end

    def edit
        @payment = Payment.find(params[:id])
    end

    def update
        @payment = Payment.find(params[:id])
        @payment.update(resources_params)
        flash[:notice] = 'Payment has been update'
        redirect_to payments_path(@exam)
    end

    def destroy
        @payment = Payment.find(params[:id])
        @payment.destroy
        flash[:notice] = 'Payment has been destroy'
        redirect_to payments_path(@exam)
    end

    def index
        @payments = Payment.all
    end
    
    def show
        id = params[:id]
        @payment = Payment.find(id)
    end

    private  
    def resources_params
        params.required(:payment).permit(:id_transaction, :status, :upload)
    end
end
