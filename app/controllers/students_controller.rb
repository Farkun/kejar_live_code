class StudentsController < ApplicationController
    def new
        @student = Student.new
    end

    def create
        student = Student.new (resources_params)
        student.save 
        redirect_to students_path
        flash[:notice] = 'Student has been created'
    end

    def edit
        @student = Student.find(params[:id])
    end

    def update
        @student = Student.find(params[:id])
        @student.update(resources_params)
        flash[:notice] = 'Student has been update'
        redirect_to student_path(@student)
    end

    def destroy
        @student = Student.find(params[:id])
        @student.destroy
        flash[:notice] = 'Student has been destroy'
        redirect_to students_path(@student)
    end

    def index
        @students = Student.all
    end
    
    def show
        id = params[:id]
        @student = Student.find(id)
    end

    private  
    def resources_params
        params.required(:student).permit(:name, :username, :age, :kelas, :address, :city, :NIK)
    end
end
