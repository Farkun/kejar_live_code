class TeachersController < ApplicationController
    def new
        @teacher = Teacher.new
    end

    def create
        teacher = Teacher.new (resources_params)
        teacher.save 
        redirect_to teachers_path
        flash[:notice] = 'Teacher has been created'
    end

    def edit
        @teacher = Teacher.find(params[:id])
    end

    def update
        @teacher = Teacher.find(params[:id])
        @teacher.update(resources_params)
        flash[:notice] = 'Teacher has been update'
        redirect_to teachers_path(@teacher)
    end

    def destroy
        @teacher = Teacher.find(params[:id])
        @teacher.destroy
        flash[:notice] = 'Teacher has been destroy'
        redirect_to teachers_path(@teacher)
    end

    def index
        @teachers = Teacher.all
    end
    
    def show
        id = params[:id]
        @teacher = Teacher.find(id)
    end

    private  
    def resources_params
        params.required(:teacher).permit(:NIK, :nama, :age, :kelas, :mapel)
    end
end
