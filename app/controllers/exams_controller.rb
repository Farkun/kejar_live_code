class ExamsController < ApplicationController
    def new
        @exam = Exam.new
    end

    def create
        exam = Exam.new (resources_params)
        exam.save 
        redirect_to exams_path
        flash[:notice] = 'Exam has been created'
    end

    def edit
        @exam = Exam.find(params[:id])
    end

    def update
        @exam = Exam.find(params[:id])
        @exam.update(resources_params)
        flash[:notice] = 'Exam has been update'
        redirect_to exams_path(@exam)
    end

    def destroy
        @exam = Exam.find(params[:id])
        @exam.destroy
        flash[:notice] = 'Exam has been destroy'
        redirect_to exams_path(@exam)
    end

    def index
        @exams = Exam.all
    end
    
    def show
        id = params[:id]
        @exam = Exam.find(id)
    end

    private  
    def resources_params
        params.required(:exam).permit(:title, :hasil, :mapel, :duration, :nilai, :status, :level, :student_id)
    end
end
