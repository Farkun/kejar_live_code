# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Student.create(name: 'rahayu',username: 'ayu',age: 17, kelas: 'rpl',address: 'jln lodaya',city: 'bekasi',NIK: '123669')
Student.create(name: 'abimanyu',username: 'manyu',age: 18, kelas: 'tkj',address: 'jln ciawi',city: 'garut',NIK: '123656')
Student.create(name: 'syahla',username: 'lala',age: 20, kelas: 'otkp',address: 'jln cisarua',city: 'depok',NIK: '123643')