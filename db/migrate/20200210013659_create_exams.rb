class CreateExams < ActiveRecord::Migration[6.0]
  def change
    create_table :exams do |t|
      t.string :title
      t.string :hasil
      t.string :mapel
      t.string :duration
      t.float :nilai
      t.string :status
      t.string :level
      t.string :student_id

      t.timestamps
    end
  end
end
