class CreateStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :students do |t|
      t.string :name
      t.string :username
      t.integer :age
      t.string :kelas
      t.text :address
      t.string :city
      t.string :NIK

      t.timestamps
    end
  end
end
